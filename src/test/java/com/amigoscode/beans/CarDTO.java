package com.amigoscode.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDTO {

    private  Integer id;
    private  String model;
    private  Integer year;
    private  Double price;

//    public static CarDTO mapCar (Car car){
//        return new CarDTO(car.getId(),
//                car.getModel(),
//                car.getYear(),
//                car.getPrice());
//    }
}
