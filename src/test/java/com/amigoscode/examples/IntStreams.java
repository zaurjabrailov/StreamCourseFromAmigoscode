package com.amigoscode.examples;


import com.amigoscode.beans.Car;
import com.amigoscode.beans.Person;
import com.amigoscode.mockdata.MockData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class IntStreams {

    @Test
    public void range() throws Exception {
        System.out.println("with for i");
        for (int i = 0; i <= 10; i++) {
            System.out.println(i);
        }
        System.out.println("with int stream exclusive");
        IntStream.range(0, 10).forEach(System.out::println);

        System.out.println("with int stream inclusive");
        IntStream.rangeClosed(0, 10).forEach(System.out::println);
    }

    @Test
    public void rangeByZaur() {
        IntStream.range(1, 5).forEach(System.out::println);  // with out 5
        System.out.println("---------------------------------------");
        IntStream.rangeClosed(1, 5).forEach(System.out::println);// with 5
    }

    // Loop through people using IntStream
    @Test
    public void rangeIteratingLists() throws Exception {
        List<Person> people = MockData.getPeople();
        IntStream.range(0, people.size())
                .limit(12)
                .forEach(index -> {
                    System.out.println(people.get(index));
                });
    }

    @Test
    public void rangeListByZaur() throws IOException {
        List<Car> cars = MockData.getCars();
        IntStream.rangeClosed(1, cars.size())
                .limit(5)
                .forEach(value -> {
                    System.out.println(cars.get(value));
                });

    }

    @Test
    public void intStreamIterate() {
        IntStream.iterate(0, value -> value + 2)
                .limit(11)
                .forEach(System.out::println);
    }
}
