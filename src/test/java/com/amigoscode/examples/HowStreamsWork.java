package com.amigoscode.examples;


import com.amigoscode.beans.Person;
import com.amigoscode.mockdata.MockData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class HowStreamsWork {
    @Test
    public void understandingCollect() throws Exception {
        List<String> emails = MockData.getPeople()
                .stream()
                .map(Person::getEmail)
                .collect(Collectors.toList());

        emails.forEach(System.out::println);
    }

    @Test
    public void intermediateAndTerminalOperations() throws Exception {
        System.out.println(
                MockData.getCars()
                        .stream()
                        .filter(car -> {
                            System.out.println("filter car " + car);
                            return car.getPrice() < 10000;
                        })
                        .map(car -> {
                            System.out.println("mapping car " + car);
                            return car.getPrice();
                        })
                        .map(price -> {
                            System.out.println("mapping price " + price);
                            return price + (price * .10);
                        })
                        .collect(Collectors.toList())
        );
    }

    @Test
    public void TerminalOperationsByZaur() throws IOException {
        System.out.println(
                MockData.getPeople()
                        .stream()
                        .filter(person -> person.getFirstName().equals("Carlyle"))
//                        .filter((Person person) -> {
//                            return person.getAge() < 5;
//                        })
                        .limit(2)
                        .collect(Collectors.toList())
        );
    }
}
