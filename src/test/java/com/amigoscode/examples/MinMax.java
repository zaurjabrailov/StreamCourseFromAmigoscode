package com.amigoscode.examples;

import com.amigoscode.beans.Car;
import com.amigoscode.mockdata.MockData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.DoubleStream;

public class MinMax {

    @Test
    public void min() {
        List<Integer> numbers = List.of(1, 2, 3, 100, 23, 93, 99);
        Integer min = numbers.stream().min(Comparator.naturalOrder()).get();
        System.out.println(min);
    }

    @Test
    public void minByZaur() throws IOException {
        List<Car> cars = MockData.getCars();
        Optional<Car> min = cars.stream()
                .min(Comparator.comparing(Car::getPrice));
        System.out.println(min);
    }

    @Test
    public void max() {
        List<Integer> numbers = List.of(1, 2, 3, 100, 23, 93, 99);
        Integer max = numbers.stream().max(Comparator.naturalOrder()).get();
        System.out.println(max);
    }

    @Test
    public void maxByZaur() throws IOException {
        List<Car> cars = MockData.getCars();
        Optional<Car> max = cars.stream()
                .max(Comparator.comparing(Car::getPrice));
        System.out.println(max);
    }
}
