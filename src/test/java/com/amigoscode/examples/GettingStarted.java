package com.amigoscode.examples;

import com.amigoscode.beans.Car;
import com.amigoscode.beans.Person;
import com.amigoscode.mockdata.MockData;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.*;


public class GettingStarted {

    @Test
    public void imperativeApproach() throws IOException {
        // 1. Find people aged less or equal 18
        // 2. Then change implementation to find first 10 people
        List<Person> people = MockData.getPeople();
        List<Person> youngPeople = new ArrayList<>();

        int limit = 10;
        int counter = 0;
        for (Person person : people) {
            if (person.getAge() <= 18) {
                youngPeople.add(person);
                counter++;
                if (counter == limit) {
                    break;
                }

            }
        }
        youngPeople.forEach(System.out::println);
    }


    @Test
    public void declarativeApproachUsingStreams() throws Exception {
        List<Person> people = MockData.getPeople();
        List<Person> youngPeople = people.stream()
                .filter(p -> p.getAge() <= 18)
                .limit(10)
                .sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());
        youngPeople.forEach(System.out::println);
    }

    @Test
    public void imperativeByZaur() throws IOException {
        // 1. Find cars years less or equal 1990
        // 2. Then change implementation to find first 10 cars
        List<Car> cars = MockData.getCars();
        List<Car> carYears = new ArrayList<>();

        int limitCar = 10;
        int counterLimit = 0;
        int totalCounter = 0;

        for (Car car : cars) {
            totalCounter++;
            if (car.getYear() <= 1990) {
                carYears.add(car);
                carYears.sort(Comparator.comparing(Car::getYear));
                counterLimit++;
                if (counterLimit == limitCar)
                    break;
            }
        }
        System.out.println(totalCounter);
        carYears.forEach(System.out::println);
    }


    @Test
    public void declarativeByZaurUsingStreams() throws Exception {

        // 1. Find cars years less or equal 1990
        // 2. Then change implementation to find first 10 cars
        List<Car> cars = MockData.getCars();
        List<Car> collectCars = cars.stream()
                .filter(car -> car.getYear() <= 1990)
                .limit(10)
                .sorted(Comparator.comparing(Car::getYear))
                .collect(Collectors.toList());

        collectCars.forEach(System.out::println);
    }
}


